import cv2
import numpy
import csv
from scipy.spatial.distance import euclidean
from skimage.io import imsave
import os.path

from utilities_lib import get_imgfiles, get_annofiles

MAXPIXEL_ERROR = 5


def get_nn_cell_pos(query_pos, pos_list):
    posmin = None
    min_err = 1e10  # very large number
    for p in pos_list:
        dist = euclidean(numpy.asarray(p), numpy.asarray(query_pos))
        if dist < min_err:
            min_err = dist
            posmin = p

    return posmin, min_err


def compute_metrics(ground_truth, predicted):
    cnt = 0

    tp = 0
    fp = 0
    fn = len(ground_truth) - len(predicted)
    if fn < 0:
        fn = 0

    neighbors = set()
    for (x, y) in predicted:
        cnt += 1

        nearest_neighbor, eps = get_nn_cell_pos((x, y), ground_truth)
        if eps < 25:
            if nearest_neighbor in neighbors:
                continue

            neighbors.add(nearest_neighbor)
            tp += 1
        else:
            fp += 1

    acc = tp / len(ground_truth)

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall)

    return acc, precision, recall, f1


def visualize_cell_positions(my_img, my_cellpos_list, ground_truth_pos_list=None, radius=2):
    output = my_img.copy()

    # loop over the (x, y) coordinates
    for (x, y) in my_cellpos_list:
        cv2.rectangle(output, (x - radius, y - radius), (x + radius, y + radius), (0, 128, 255), -1)

    if ground_truth_cellpos_list:
        for (x, y) in ground_truth_cellpos_list:
            cv2.rectangle(output, (x - radius, y - radius), (x + radius, y + radius), (255, 128, 0), -1)

    # show the output image
    # cv2.imshow("output", output)
    # cv2.waitKey(0)

    return output


def load_cell_positions(cellpos_fname):
    # format is (X, Y)
    cellpos_list = []
    with open(cellpos_fname, 'r') as f:
        cellposreader = csv.reader(f, delimiter='	')

        cnt = 0
        for row in cellposreader:
            if cnt == 0:
                cnt += 1
                continue

            cellpos_list.append((int(row[1]), int(row[2])))

    return cellpos_list


def compute_cell_positions(my_img, image_type):
    cellpos_list = None

    # load the image and then convert it to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    if image_type == 'oblique':
    # detect cells in the image
    print('oblique')

    elif image_type == 'phase':
    # detect cells in the image
    print('phase')

    return cellpos_list


if __name__ == "__main__":
    all_anno = get_annofiles('dataset/annotations/')
    all_oblique = get_imgfiles('dataset/oblique/')
    all_phase = get_imgfiles('dataset/phase/')

    for image_type in ('oblique', 'phase'):
        tot_acc = 0
        tot_precision = 0
        tot_recall = 0
        tot_f1 = 0

        if image_type == 'oblique':
            image_files = all_oblique
            else:
            image_files = all_phase

        for annopath, image_path in zip(all_anno, image_files):
            image = cv2.imread(image_path)

            ground_truth_cellpos_list = load_cell_positions(annopath)

            ## Compute cell positions here
            predicted_cellpos_list = compute_cell_positions(image, image_type)

            # OPTIONAL: visualize results
            # output = visualize_cell_positions(image, predicted_cellpos_list)
            # overlay_fname = os.path.join('results', os.path.basename(image_path).split('.')[0] + '_overlay.png')
            # imsave(overlay_fname, output)

            acc, precision, recall, f1 = compute_metrics(ground_truth_cellpos_list, predicted_cellpos_list)

            tot_acc += acc
            tot_precision += precision
            tot_recall += recall
            tot_f1 += f1

        tot_acc /= len(all_anno)
        tot_precision /= len(all_anno)
        tot_recall /= len(all_anno)
        tot_f1 /= len(all_anno)

        print("Accuracy " + str(tot_acc))
        print("Precision " + str(tot_precision))
        print("Recall " + str(tot_recall))
        print("F1 " + str(tot_f1))
