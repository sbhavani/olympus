from os.path import isdir, basename, join, splitext
from glob import glob
from numpy import argsort, random
import datetime
import hashlib
from functools import partial
import subprocess
import boto
from boto.s3.key import Key
from urllib.request import urlopen
from io import BytesIO
from PIL import Image

# SemanticMD libraries
import app.ml.settings as settings

# supported image extensions
EXTENSIONS = [".jpeg", ".jpg", ".bmp", ".png", ".pgm", ".tif", ".tiff", ".svs"]
ANNOTATION_EXTENSIONS = [".txt", ".json"]

def get_categories(datasetpath):
    cat_paths = [files
                 for files in glob(datasetpath + "/*")
                 if isdir(files)]
    cat_paths.sort()
    cats = [basename(cat_path) for cat_path in cat_paths]
    return cats


# returns a list containing image file paths
def get_imgfiles(path):
    all_files = []
    all_files.extend([join(path, basename(fname))
                      for fname in glob(path + "/*")
                      if splitext(fname)[-1].lower() in EXTENSIONS])
    return all_files


# returns a list containing annotation file paths
def get_annofiles(path):
    all_files = []
    all_files.extend([join(path, basename(fname))
                      for fname in glob(path + "/*")
                      if splitext(fname)[-1].lower() in ANNOTATION_EXTENSIONS])
    return all_files


def merge_two_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.'''
    z = x.copy()
    z.update(y)
    return z


def top_k_max_elements(arr, k):
    return argsort(-arr)[:k]


# Source: http://stackoverflow.com/questions/6482889/get-random-sample-from-list-while-maintaining-ordering-of-items
def random_sample_from_list(list_, sample_size):
    random.seed(1984)
    return [list_[i] for i in random.choice(range(len(list_)), size=sample_size)]


# returns the current date and time as an ISO8601 formatted string
def get_system_time():
    current_time = datetime.datetime.now()
    return current_time.isoformat()


def file_hash(filedata):
    d = hashlib.sha1()
    for buf in iter(partial(filedata.read, 128), b''):
        d.update(buf)
    return d.hexdigest()


def url2image(my_url):
    file = BytesIO(urlopen(my_url).read())
    img = Image.open(file)

    return img
